﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GridMaster;
using Game.Core.Combat;

public class AttackTimeline : MonoBehaviour {

    public int timeline = 100;

    [SerializeField]
    Slider timelineIndicator;
    [SerializeField]
    Animator endTurnIndicator;

    public List<Attack> attacks = new List<Attack>();

    public static AttackTimeline Instance { get; private set; }

    [Tooltip("This Gradient indicates the attack time. 0% => attack has just been made. 100% => Attack is just about to go off")]
    [SerializeField]
    Gradient attackColorGradient;

    bool showIndicator;


    private void Awake() {
        Instance = this;
    }

    public void ExecuteActions() {
        Debug.Log("Executing actions..");
        // Here we execute all the actions the player or the enemy has layed out on their turn

        // TODO Change this to function which runs when player moves or attacks
        timelineIndicator.value = timeline;

        // TODO Set end turn indicator on when we cannot make moves anymore
        if (timeline <= 0) {
            ToggleEndTurnIndicator();
        }

        for (int i = 0; i < attacks.Count; i++) {
            for (int x = 0; x < attacks[i].attackNodes.Count; x++) {
                float timeCalc = (100 - timeline) * .01f;
                attacks[i].attackNodes[x].worldObject.GetComponent<Renderer>().material.color = attackColorGradient.Evaluate(timeCalc);
            }
        }

        // Clear the attack list upon completion
        attacks.Clear();
        // And switch the turn to the approprtiate party
        CombatStateMachine.Instance.SwitchState(CombatStateMachine.BattleState.PLAYER);
    }

    public void ToggleEndTurnIndicator() {
        showIndicator = !endTurnIndicator.GetBool("EndTurn");
        endTurnIndicator.SetBool("EndTurn", showIndicator);
    }

    public void EndTurn() {
        Debug.Log("Turn ended");
        ToggleEndTurnIndicator();
        CombatStateMachine.Instance.SwitchState(CombatStateMachine.BattleState.EXECUTE);
    }

    public void CancelActions() {
        ToggleEndTurnIndicator();
        Debug.Log("All actions from the turn has been cancelled");
        // TODO Clear all the actions made by the player
    }
}
