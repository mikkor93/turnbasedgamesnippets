﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GridMaster;
using System;

namespace Game.Core.Combat {
    public class EnemyController : MonoBehaviour {

        [SerializeField]
        GameObject enemy;
        [SerializeField]
        int numOfEnemies = 1;

        [SerializeField]
        List<int> enemyPlaces = new List<int>();

        public List<GameObject> enemies = new List<GameObject>();

        public static EnemyController Instance { get; private set; }


        private void Awake() {
            Instance = this;
        }


        void Start() {
            PlaceEnemies();
        }

        public void StartAction() {
            Debug.Log("Enemy action begins here");

            // This is the beginning of the enemy AI
            // For now we just move the enemy
            //for (int i = 0; i < enemies.Count; i++) {
            //    enemies[i].transform.position += transform.right * 1;
            //}

            CombatStateMachine.Instance.SwitchState(CombatStateMachine.BattleState.PLAYER);
        }

        void BackToPlayerTurn() {
            CombatStateMachine.Instance.SwitchState(CombatStateMachine.BattleState.PLAYER);
        }

        private void PlaceEnemies() {
            List<Node> nodes = GridBase.instance.nodes;
            for (int i = 0; i < nodes.Count; i++) {
                if (enemyPlaces.Contains(nodes[i].id)) {
                    GameObject go = Instantiate(enemy) as GameObject;
                    go.transform.position = nodes[i].worldObject.transform.position;
                    go.transform.SetParent(nodes[i].worldObject.transform);
                    enemies.Add(go);
                }
            }
        }
    }
}