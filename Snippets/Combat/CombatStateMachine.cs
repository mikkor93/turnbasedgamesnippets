﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game.Core.Combat {
    public class CombatStateMachine : MonoBehaviour {

        public PlayerActionManager playerActionManager;

        AttackTimeline timeline;


        public enum BattleState {
            START,
            PLAYER,
            ENEMY,
            EXECUTE,
            WIN,
            LOSE
        }

        public BattleState state = BattleState.START;

        public static CombatStateMachine Instance { get; private set; }


        private void Awake() {
            Instance = this;

            timeline = GetComponent<AttackTimeline>();
        }

        public void SwitchState(BattleState stateToSwitch, Attack attack = null) {
            state = stateToSwitch;
            // Stop the attack timer
            AttackTimeManager.Instance.timerEnabled = false;

            switch (state) {
                case BattleState.START:
                    break;

                case BattleState.PLAYER:
                    // Reset the important values
                    playerActionManager.ResetActions();
                    // Reset the board
                    //<list>GridMaster.Node n GridMaster.GridBase.instance.nodes;
                    List<GridMaster.Node> nodes = GridMaster.GridBase.instance.nodes;
                    for (int i = 0; i < nodes.Count; i++) {
                        nodes[i].worldObject.GetComponent<Renderer>().material.color = new Color(0, 255, 255, 255);
                    }
                    break;

                case BattleState.EXECUTE:
                    // Execute all the actions 
                    timeline.ExecuteActions();
                    break;

                case BattleState.ENEMY:
                    EnemyController.Instance.StartAction();
                    break;

                case BattleState.WIN:
                    break;

                case BattleState.LOSE:
                    break;

                default:
                    Debug.LogWarning("Battlestate not found!");
                    break;
            }

            Debug.Log("State: " + state);
        }
    }
}
