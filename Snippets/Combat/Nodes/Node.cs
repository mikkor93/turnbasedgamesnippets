﻿using UnityEngine;
using System.Collections;

namespace Game.Pathfinding {
    [System.Serializable]
    public class Node : MonoBehaviour {

        // Node's position on the grid
        public int id;
        public Vector3 worldPosition;
        public int posX;
        public int posY;

        public GameObject occupiedBy = null;

        // TODO maybe find out better solution in the future
        public bool hasEnemy = false;

        // Node's costs
        public int hCost;
        public int gCost;

        public float fCost {
            get {
                return gCost + hCost;
            }
        }

        public Node parentNode;
        public bool isWalkable = true;

        // Reference to the world object 
        public GameObject worldObject;

        // Types of nodes we can have
        public NodeType nodeType;
        public enum NodeType {
            Normal,
            Fast,
            Slow
        }
    }
}
