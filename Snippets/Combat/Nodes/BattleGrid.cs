﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//using Game.Tests.Pathfinding;

namespace Game.Pathfinding {
    public class BattleGrid : MonoBehaviour {

        [Header("FOR DEBUGGING==================")]        
        // Remove
        public GameObject enemy;
        //public Game.Tests.Pathfinding.Pathfinding pathfinding;  // Clean this up a bit mate?
        //public Transform player;
        public List<int> enemyTileIDs = new List<int>();
        [Header("===============================")]

        [Range(0, 20)]
        public int allowedTilesX = 1;
        [Range(0, 10)]
        public int allowedTilesY = 2;

        [SerializeField]
        public int sizeX = 20;
        [SerializeField]
        public int sizeY = 10;
        [SerializeField]
        float scaleX = 1;
        [SerializeField]
        float scaleY = 1;        

        [SerializeField]
        Node[,] grid;
        [SerializeField]
        public List<Node> nodes = new List<Node>();
        [SerializeField]
        Node targetNode;

        GameObject gridParent;
        [SerializeField]
        GameObject nodeObj;

        public static BattleGrid singleton;

        private void Awake() {
            singleton = this;
        }

        private void Start() {
            Init();
        }

        public void Init() {
            gridParent = new GameObject();
            gridParent.name = "Grid";

            CreateGrid();

            //Check();

            // Create debug text and add id to node
            for (int i = 0; i < nodes.Count; i++) {
                nodes[i].id = i;
                //nodes[i].GetComponentInChildren<TextMesh>().text = i.ToString();

                //if (enemyTileIDs.Contains(nodes[i].id)) {
                //    GameObject e = Instantiate(enemy, new Vector3(nodes[i].worldPosition.x, nodes[i].worldPosition.y + 0.25f, nodes[i].worldPosition.z), Quaternion.identity);
                //    nodes[i].hasEnemy = true;
                //    nodes[i].occupiedBy = e;
                //    e.transform.SetParent(nodes[i].transform);
                //}
            }
        }

        private void CreateGrid() {
            grid = new Node[sizeX, sizeY];

            for (int x = 0; x < sizeX; x++) {
                for (int y = 0; y < sizeY; y++) {
                    Vector3 targetPos = WorldCoordinatesFromNode(x, 0, y);
                    GameObject node = Instantiate(nodeObj, targetPos, Quaternion.identity) as GameObject;
                    node.transform.eulerAngles = new Vector3(90, 0 , 0);
                    node.name = "Node (" + x + " , " + y + ")";
                    node.AddComponent<Node>();

                    Node n = node.GetComponent<Node>();
                    n.posX = x;
                    n.posX = 0;
                    n.posX = y;
                    n.isWalkable = true;
                    n.worldPosition = new Vector3(x, 0, y);                    

                    node.transform.parent = gridParent.transform;
                    n.worldObject = node;

                    grid[x, y] = n;                       

                    nodes.Add(n);
                }
            }
        }

        public Vector3 WorldCoordinatesFromNode(int x, int y, int z) {
            Vector3 r = Vector3.zero;
            r.x = x * scaleX;
            r.y = y * scaleY;
            r.z = z * scaleY;            
            return r;
        }

        private void Check() {
            if(sizeX == 0) {
                Debug.Log("Size x is 0, assigning min");
                sizeX = 10;
            }

            if (sizeY == 0) {
                Debug.Log("Size y is 0, assigning min");
                sizeY = 1;
            }

            if (scaleX == 0) {
                Debug.Log("Scale x is 0, assigning min");
                sizeX = 1;
            }

            if (scaleY == 0) {
                Debug.Log("Scale y is 0, assigning min");
                sizeX = 1;
            }
        }

        public Node GetNode(int x, int y, int z) {

            x = Mathf.Clamp(x, 0, sizeX - 1);
            y = Mathf.Clamp(y, 0, sizeY - 1);
            z = Mathf.Clamp(z, 0, sizeY - 1);

            return grid[x, z];

            //Used to get a node from a grid,
            //If it's greater than all the maximum values we have
            //then it's going to return null

            //float percentX = (worldPosition.x + sizeX / 2) / sizeX;
            //float percentY = (worldPosition.x + sizeY / 2) / sizeY;
            //percentX = Mathf.Clamp01(percentX);
            //percentY = Mathf.Clamp01(percentY);

            //int x = Mathf.RoundToInt((sizeX - 1) * percentX);
            //int y = Mathf.RoundToInt((sizeY - 1) * percentY);
            //return grid[x, y];


            //Node retVal = null;

            //if (worldPosition.x < sizeX && worldPosition.x >= 0 && worldPosition.y >= 0 && worldPosition.y < sizeY) {
            //    retVal = grid[(int)worldPosition.x, (int)worldPosition.y];
            //}

            //return retVal;
        }

        public List<Node> GetNeighbours(Node node) {
            List<Node> neighbours = new List<Node>();

            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if (x == 0 && y == 0) {
                        continue;
                    }

                    int checkX = node.posX + x;
                    int checkY = node.posX + y;

                    if (checkX >= 0 && checkX < sizeX && checkY >= 0 && checkY < sizeY) {
                        neighbours.Add(grid[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }

        public List<Node> GetAvailabeNodes() {
            List<Node> availableNodes = new List<Node>();

            for (int i = 0; i <= nodes.Count - 1; i++) {
                Node n = nodes[i];
                if(n.worldPosition.x < allowedTilesX && n.worldPosition.z < allowedTilesY) {
                    //n.worldObject.GetComponentInChildren<Renderer>().material.color = Color.black;

                    availableNodes.Add(n);
                }
                else {
                    //n.worldObject.GetComponentInChildren<Renderer>().material.color = Color.blue;
                }
            }

            return availableNodes;
        }

        //public List<Node> path;
        //private void OnDrawGizmos() {
        //    if(grid != null) {
        //        foreach (Node n in grid) {
        //            Gizmos.color = (n.isWalkable) ? Color.white : Color.red;
        //            if(path != null) {
        //                if(path.Contains(n)) {
        //                    Gizmos.color = Color.black;
        //                }
        //            }
        //            Gizmos.DrawCube(n.worldPosition, Vector3.one * (0.25f));
        //        }
        //    }
        //}


    }

    [Serializable]
    public class YLevels {
        public int y;
        public GameObject nodeParent;
        public GameObject collisionObj;
    }
}
