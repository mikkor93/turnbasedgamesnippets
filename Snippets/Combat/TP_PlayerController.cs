﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Game.Pathfinding;
using Game.Core.Combat;
using GridMaster;
using System;

[RequireComponent(typeof(PlayerActionManager))]
public class TP_PlayerController : MonoBehaviour {

    [Header("Player States===================")]

    PlayerActionManager actionManager;
    [Header("================================")]

    [SerializeField]
    Image staminaBar;
    float stamina = 1;

    public float speed = 2f;
    public int attackLength = 3;

    public GridBase grid;

    public List<GridMaster.Node> path;
    public GameObject currentNode;

    Transform previousTile;
    public Vector3 attackHitPos;

    [Range(1, 23)]
    public int walkableX;
    [Range(1, 23)]
    public int walkableY;


    public LineRenderer line;
    public int pathLength;


    private void Start() {
        actionManager = GetComponent<PlayerActionManager>();

        ColorTiles(walkableX, walkableY);
    }

    void Update() {
        // Return from the update loop if player has no turn
        if (CombatStateMachine.Instance.state != CombatStateMachine.BattleState.PLAYER) return;

        float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float y = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        ClampMovement(walkableX, walkableY);
        Movement(x, y);

        CheckTile();

        // TODO: make these attacks more modular and easy to add
        if (Input.GetKeyDown(KeyCode.Alpha1) && !actionManager.HasActionBeenUsed(1) && AttackTimeline.Instance.timeline >= attackLength) {        

            // TODO: Make some sort of function for creating an attack
            // Add the attack to the attacks list
            Attack newAttack = new Attack();
            newAttack.time = 5;
            newAttack.totalDuration = 5;
            newAttack.parent = this.gameObject;
            
            for (int i = 0; i <= attackLength; i++) {
                GridMaster.Node n = grid.GetNodeFromVector3(transform.position + (-Vector3.left * i));
                n.worldObject.GetComponent<Renderer>().material.color = Color.red;

                newAttack.attackNodes.Add(n);
            }

            //AttackTimeManager.Instance.attacks.Add(newAttack);
            AttackTimeline.Instance.attacks.Add(newAttack);
            AttackTimeline.Instance.timeline -= (attackLength * 10);

            //CombatStateMachine.Instance.SwitchState(CombatStateMachine.BattleState.ENEMY);

            actionManager.UseAction(1);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2)) {

            Debug.Log("Drank a health potion. It gave you +10 Health");

            actionManager.UseAction(2);
        }
    }

    private void Movement(float x, float y) {
        if(!actionManager.HasActionBeenUsed(1)) {
            // Reduce the time when we are moving
            if (Mathf.Abs(x) > 0 || Mathf.Abs(y) > 0) {
                if (AttackTimeline.Instance.timeline >= 0) {
                    Pathfinding.PathfindMaster.GetInstance().RequestPathfind(
                        GridBase.instance.GetNode(0, 0, 0), // TODO use the node the player is at the start of the turn
                        GridBase.instance.GetNode((int)currentNode.transform.position.x,
                                                    (int)currentNode.transform.position.y,
                                                    (int)currentNode.transform.position.z),
                        RequestedPath);


                    if (AttackTimeline.Instance.timeline > 0) {
                        AttackTimeline.Instance.timeline = (100 - (pathLength * 10));
                    }

                    transform.Translate(x, 0, y);
                }
            }
        }
    }

    private void RequestedPath(List<GridMaster.Node> path) {
        line.positionCount = path.Count;
        pathLength = path.Count;
        for (int i = 0; i < path.Count; i++) {
            line.SetPosition(i, path[i].worldObject.transform.position);
        }
    }

    private void ClampMovement(int maxX, int maxY) {
        Vector3 curPos = transform.position;
        curPos.x = Mathf.Clamp(curPos.x, 0, maxY);
        curPos.z = Mathf.Clamp(curPos.z, 0, maxY);
        transform.position = curPos;                
    }

    private void ColorTiles(int x, int z) {
        // Color the tiles we cant move
        // TODO Maybe delete this?
        // TODO if you decide to use this, optimize, this is horrible
        for (int i = 0; i < GridMaster.GridBase.instance.nodes.Count; i++) {
            GridMaster.Node n = GridMaster.GridBase.instance.nodes[i];
            if (n.x <= x && n.z <= z) {
                n.worldObject.GetComponent<Renderer>().material.color = Color.green;
            }
        }
    }

    private void CheckTile() {
        RaycastHit hit;
        Debug.DrawLine(transform.position, -transform.up * 2f, Color.red);
        if(Physics.Raycast(transform.position, -transform.up * 2f, out hit)) {
            if(previousTile != null) {
                //previousTile.GetComponent<Renderer>().material.color = originalColor;
            }

            GameObject hitObj = hit.collider.gameObject;
            //hitObj.GetComponent<Renderer>().material.color = Color.red;
            currentNode = hitObj;

            // Make the current tile the player is on, it's parent
            transform.SetParent(hitObj.transform);

            previousTile = hit.collider.transform;
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, Vector3.one * .5f);
        if (attackHitPos != null) {
            Gizmos.DrawSphere(attackHitPos, .5f);
        }
    }
}
