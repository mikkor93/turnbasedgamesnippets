﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour {

    public enum BattleState {
        START,
        PLAYER,
        PLAYERANIMATE,
        ENEMY,
        WIN,
        LOSE
    }

	[SerializeField]
	GameObject targetArrow;
	GameObject currentTargetArrow;

    private BattleState state;


	void Update () {
		SwitchState(BattleState.PLAYER	);
	}	

	private void SwitchState (BattleState stateToSwitch) {
		switch (stateToSwitch) {
			case BattleState.PLAYER:
				PlayerTurn();	
				Debug.Log("Player's Turn");			
				break;

			default:
				Debug.LogError("State not found!");
				break;
		}
	}

	private void PlayerTurn() {	
		if (Input.GetMouseButtonDown(0)) {
            RaycastHit2D hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			hit = Physics2D.Raycast(ray.origin, ray.direction);
             
			Vector2 targetPos = hit.collider.transform.position;
			if(currentTargetArrow != null) {
				// TODO: Cache
				Destroy(currentTargetArrow);
				currentTargetArrow = null;
			}
			currentTargetArrow = Instantiate(targetArrow, new Vector2(targetPos.x, targetPos.y + 2f), Quaternion.identity) as GameObject;
        }
	}
}
