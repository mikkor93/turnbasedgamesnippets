﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

using GridMaster;

namespace Game.Core.Combat {
    public class AttackTimeManager : MonoBehaviour {

        [Header("FOR DEBUGGING==================")]
        public int attacksToSpawn = 5;
        public GameObject enemy;
        public bool timerEnabled = true;
        public GridBase grid;
        [Header("===============================")]

        public int timeline = 100;
        public int TEST_AttackTime = 50;

        [Header("UI Buttons=====================")]
        [SerializeField]
        Image[] attackButtons;
        [Header("===============================")]

        public List<Attack> attacks = new List<Attack>();


        public static AttackTimeManager Instance { get; private set; }


        private void Awake() {
            Instance = this;
        }

        void Start() {
            // Add some template attack times            

            StartCoroutine(TimerTick());

            Attack a = new Attack();
            a.time = 5;
            a.totalDuration = 5;
            //a.parent = this.gameObject;

            for (int i = 0; i <= 3; i++) {
                GridMaster.Node n = grid.GetNodeFromVector3(-Vector3.left * i);
                a.attackNodes.Add(n);
            }
            attacks.Add(a);
        }

        IEnumerator TimerTick() {
            while(true) {
                // Count down the timer when we are moving the time between attacks
                if (true) {
                    ReduceAttackTime();
                    SortAttackList();                    

                    // TODO Loop through the entire list of attacks and see which ones have timers at 0
                    for (int i = 0; i < attacks.Count; i++) {

                        // When the attacks timer has reached 0
                        if (attacks[i].time < 0) {
                            attacks[i].time = attacks[i].totalDuration;

                            // Do Damage
                            Damage(attacks[i]);

                            // Remove the attack from the list
                            //attacks.Remove(attacks[i]);
                        }
                    }                    
                }

                IndicateAttackTime();

                Debug.Log("timer tick");
                yield return new WaitForSeconds(1f);    // We tick every 1 seconds
            }
        }

        private void Damage(Attack attack) {
            for (int i = 0; i < attack.attackNodes.Count; i++) {               

                DEMO_Health health = attack.attackNodes[i].worldObject.GetComponentInChildren<DEMO_Health>();
                // Do damage on tiles where there is enemy on it
                if(health != null) {
                    health.TakeDamage(1);
                }
            }
        }

        /// <summary>
        /// Change the color of the attack nodes over time, increasing in color tencity nearing the attack time
        /// </summary>
        private void IndicateAttackTime() {
            for (int x = 0; x < attacks[0].attackNodes.Count; x++) {
                Gradient g;
                GradientColorKey[] gck;
                GradientAlphaKey[] gak;
                g = new Gradient();
                gck = new GradientColorKey[2];
                gck[0].color = Color.red;
                gck[0].time = 0.0F;
                gck[1].color = Color.blue;
                gck[1].time = 1.0F;
                gak = new GradientAlphaKey[2];
                gak[0].alpha = 1.0F;
                gak[0].time = 0.0F;
                gak[1].alpha = 0.0F;
                gak[1].time = 1.0F;
                g.SetKeys(gck, gak);

                attacks[0].attackNodes[x].worldObject.GetComponent<Renderer>().material.color = new Color(
                    g.Evaluate(Time.deltaTime).r,
                    g.Evaluate(Time.deltaTime).g,
                    g.Evaluate(Time.deltaTime).b);
            }
        }

        /// <summary>
        /// Reduce the timer from each attack by 1
        /// </summary>
        private void ReduceAttackTime() {
            foreach (Attack a in attacks) {
                a.time--;
            }
        }

        /// <summary>
        /// Sort the list if attacks from smallest swing time to biggest
        /// </summary>
        private void SortAttackList() {
            attacks = attacks.OrderBy(go => go.time).ToList();
        }
    }
}
