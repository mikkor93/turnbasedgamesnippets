﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GridMaster;

namespace Game.Core.Combat {
    [System.Serializable]
    public class Attack  {

        public GameObject parent;
        public int time;
        public int totalDuration;

        public int placeInTimeline;
        public int timeCost;

        public List<Node> attackNodes = new List<Node>();
    }
}
